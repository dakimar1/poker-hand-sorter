# Poker Hand Sorter

Program that takes, via STDIN, a "stream" of hands for a two player poker game. At the completion of the stream, the program print to STDOUT the number of hands won by Player 1, and the number of hands won by Player 2

## Getting Started

Clone the project

### install

first step:

```
mvn install
```

second step :

```
mvn package
```

## Running the tests

Go inside target and then execute :

```
 java -jar poker-hand-sorter-1.0.jar ./../src/test/resources/fileTest.txt 
```
