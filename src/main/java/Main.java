import project.exception.PokerException;
import project.manager.GameManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, PokerException {
        if (args[0].endsWith("txt")) {
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            GameManager.playGame(reader);
            reader.close();
        }else {
            GameManager.playGame(args[0]);
        }
    }
}

