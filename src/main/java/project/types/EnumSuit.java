package project.types;

public enum EnumSuit {

    DIAMONDS("D"),
    HEARTS("H"),
    SPADES("S"),
    CLUBS("C");

    private final String suit;

    /**
     * @param suit
     */
    EnumSuit(final String suit) {
        this.suit = suit;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return suit;
    }

    public static EnumSuit valueOfLabel(String label) {
        for (EnumSuit e : values()) {
            if (e.suit.equals(label)) {
                return e;
            }
        }
        return null;
    }



}
