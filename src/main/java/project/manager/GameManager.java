package project.manager;



import project.exception.PokerException;
import project.model.Hand;
import project.model.HandCombinations;

import java.io.BufferedReader;
import java.io.IOException;

public class GameManager {

    public static final String PATHERN_STDIN = "([1-9TKQJA][DHSC]\\s){9}[1-9TKQJA][DHSC]";

    private Integer scorePlayer1 = 0;
    private Integer scorePlayer2 = 0;


    public static void playGame(BufferedReader reader) throws IOException, PokerException {
        GameManager gameManager = new GameManager();
        String currentLine = null;
        while ((currentLine = reader.readLine()) != null) {
            if (!currentLine.matches(GameManager.PATHERN_STDIN)) {
                throw new PokerException("Problem when tryin to read the expression : " + currentLine);
            }
            gameManager.playTour(currentLine);
        }
        gameManager.diplayScore();
    }

    public static void playGame(String currentLine) throws PokerException {
        if (!currentLine.matches(GameManager.PATHERN_STDIN)) {
            throw new PokerException("Problem when tryin to read the expression : " + currentLine);
        }
        GameManager gameManager = new GameManager();
        gameManager.playTour(currentLine);
        gameManager.diplayScore();
    }




    public void playTour(String cards) {
        Hand hand1 = new Hand(cards.substring(0,14));
        Hand hand2 = new Hand(cards.substring(15,29));
        CombinationManager combinationManager = new CombinationManager();
        HandCombinations handCombinations1 = combinationManager.getHandCombination(hand1);
        HandCombinations handCombinations2 = combinationManager.getHandCombination(hand2);
        if (handCombinations1.compareTo(handCombinations2)>0) {
            scorePlayer1++;
        }else {
            scorePlayer2++;
        }
    }

    public void diplayScore() {
        System.out.println("Player 1: " + scorePlayer1);
        System.out.println("Player 2: " + scorePlayer2);

    }


}
