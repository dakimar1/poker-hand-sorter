package project.manager;


import project.comparator.SortCardByValue;
import project.model.Hand;

import java.util.Collections;

public class HandManager {

    public HandManager() {
    }

    public static Hand sortHand(Hand hand) {
        Collections.sort(hand.getCards(), new SortCardByValue());
        return hand;
    }




}
