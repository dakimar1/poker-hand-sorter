package project.manager;

import project.comparator.SortCombinationByRank;
import project.model.Combination;
import project.model.Hand;
import project.model.HandCombinations;
import project.types.EnumNameCombination;
import project.types.EnumValue;

import java.util.Collections;

public class CombinationManager {

    /**
     *
     * @param hand
     * @return HandCombinations
     */
    public HandCombinations getHandCombination(Hand hand) {
        hand = HandManager.sortHand(hand);
        HandCombinations handCombinations =  new HandCombinations();
        if (isRoyalFlush(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isStraightFlush(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isFourOfAKind(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isFullHouse(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isFlush(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isStraight(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isThreeOfKind(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isTwoPair(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isPair(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }else if (isHighCard(hand,handCombinations)) {
            return sortCombination(handCombinations);
        }
        return sortCombination(handCombinations);
    }

    /**
     *
     * @param handCombinations
     * @return
     */
    public HandCombinations sortCombination(HandCombinations handCombinations) {
        Collections.sort(handCombinations.getListCombination(), new SortCombinationByRank());
        return handCombinations;
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isRoyalFlush(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().equals(EnumValue.TEN) &&
                hand.getCards().get(1).getValue().equals(EnumValue.JACK) &&
                hand.getCards().get(2).getValue().equals(EnumValue.QUEEN) &&
                hand.getCards().get(3).getValue().equals(EnumValue.KING) &&
                hand.getCards().get(4).getValue().equals(EnumValue.ACE)) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.ROYAL_FLUSH, EnumValue.ACE));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isStraightFlush(Hand hand, HandCombinations handCombinations) {
        if (isStraight(hand, handCombinations) && isFlush(hand, handCombinations)) {
            handCombinations.getListCombination().clear();
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.STRAIGHT_FLUSH, hand.getCards().get(4).getValue()));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isFourOfAKind(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal()) &&
                hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.FOUR_OF_A_KIND, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal()) &&
                hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.FOUR_OF_A_KIND, hand.getCards().get(4).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            return true;
        } else {
            return false;
        }

    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isFullHouse(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.FULL_HOUSE, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.FULL_HOUSE, hand.getCards().get(0).getValue()));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isFlush(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal() - 1) &&
                hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal() - 1) &&
                hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal() - 1) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal() - 1)) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.FLUSH, hand.getCards().get(4).getValue()));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isStraight(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getSuits().equals(hand.getCards().get(1).getSuits()) &&
                hand.getCards().get(1).getSuits().equals(hand.getCards().get(2).getSuits()) &&
                hand.getCards().get(2).getSuits().equals(hand.getCards().get(3).getSuits()) &&
                hand.getCards().get(3).getSuits().equals(hand.getCards().get(4).getSuits())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.STRAIGHT, hand.getCards().get(4).getValue()));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param hand
     * @param handCombinations
     * @return
     */
    private Boolean isThreeOfKind(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.THREE_OF_A_KING, hand.getCards().get(4).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(1).getValue()));
            return true;

        } else if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.THREE_OF_A_KING, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else {
            return false;
        }
    }

    private Boolean isTwoPair(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(2).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(1).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            return true;
        } else if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal()) &&
                hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.TWO_PAIRS, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(2).getValue()));
            return true;
        } else {
            return false;
        }
    }

    private Boolean isPair(Hand hand, HandCombinations handCombinations) {
        if (hand.getCards().get(0).getValue().getVal().equals(hand.getCards().get(1).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.PAIR, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(2).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(1).getValue().getVal().equals(hand.getCards().get(2).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.PAIR, hand.getCards().get(1).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(2).getValue().getVal().equals(hand.getCards().get(3).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.PAIR, hand.getCards().get(2).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(1).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
            return true;
        } else if (hand.getCards().get(3).getValue().getVal().equals(hand.getCards().get(4).getValue().getVal())) {
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.PAIR, hand.getCards().get(3).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(1).getValue()));
            handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(2).getValue()));
            return true;
        } else {
            return false;
        }
    }

    private Boolean isHighCard(Hand hand, HandCombinations handCombinations) {
        handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(0).getValue()));
        handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(1).getValue()));
        handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(2).getValue()));
        handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(3).getValue()));
        handCombinations.getListCombination().add(new Combination(EnumNameCombination.HIGH_CARD, hand.getCards().get(4).getValue()));
        return true;
    }

}
