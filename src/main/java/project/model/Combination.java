package project.model;

import project.types.EnumNameCombination;
import project.types.EnumValue;

import java.util.Objects;

public class Combination  implements  Comparable<Combination> {

    private EnumNameCombination name;

    private EnumValue value;

    public Combination(EnumNameCombination name, EnumValue value) {
        this.name = name;
        this.value = value;
    }

    public EnumNameCombination getName() {
        return name;
    }

    public void setName(EnumNameCombination name) {
        this.name = name;
    }

    public EnumValue getValue() {
        return value;
    }

    public void setValue(EnumValue value) {
        this.value = value;
    }


    public int compareTo(Combination o) {
        if (this.getName().getRank().equals(o.getName().getRank())) {
            return this.getValue().getVal().compareTo(o.getValue().getVal());
        }else {
            return this.getName().getRank().compareTo(o.getName().getRank());
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Combination that = (Combination) o;
        return name.getRank().equals(that.name.getRank()) &&
                value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }
}
