package project.model;

import java.util.ArrayList;
import java.util.List;

public class Hand {

    private List<Card> cards = null;



    public Hand(String card) {
        cards = new ArrayList<>();
        cards.add(new Card(card.substring(0,2)));
        cards.add(new Card(card.substring(3,5)));
        cards.add(new Card(card.substring(6,8)));
        cards.add(new Card(card.substring(9,11)));
        cards.add(new Card(card.substring(12,14)));
    }


    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
