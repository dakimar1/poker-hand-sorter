package project.model;

import project.types.EnumSuit;
import project.types.EnumValue;

public class Card {

    private EnumSuit suits;

    private EnumValue value;

    public Card(String card) {
        value = EnumValue.valueOfLabel(card.substring(0,1));
        suits = EnumSuit.valueOfLabel(card.substring(1,2));
    }

    public Card(EnumSuit suits, EnumValue value) {
        this.suits = suits;
        this.value = value;
    }


    public EnumSuit getSuits() {
        return suits;
    }

    public EnumValue getValue() {
        return value;
    }

    public boolean valueEquals(EnumValue v) {
        return value.getVal().equals(v.getVal());
    }

}
