package project.exception;

public class PokerException extends Exception{

    public PokerException(String errorMessage) {
        super(errorMessage);
    }

}
